# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Name to specify for ticket_tracker_roundup',
    # 'name_de_DE': '',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Description to specify for ticket_tracker_roundup
    ''',
    # 'description_de_DE': '''
    #''',
    'depends': [
        'party_type',
        'ticket_tracker',
    ],
    'xml': [
        'company.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
