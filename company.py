# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Employee(ModelSQL, ModelView):
    _name = 'company.employee'

    roundup_user = fields.Char('Roundup User', help='The roundup username '
        'of this employee.')

Employee()

